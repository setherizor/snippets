/////////////////////////////////
//       Websockets Logic      //
/////////////////////////////////
const loc = window.location
let uri = loc.protocol === 'https:' ? 'wss:' : 'ws:'
uri += '//' + loc.host
uri += loc.pathname + 'ws'
let firstTime = true

const setupSocket = () => {
  ws = new WebSocket(uri)
  
  ws.onopen = () => {
    console.log('Socket Connected')
    $('#note').disabled = false
    $('#error').innerText = ''
  }
  
  ws.onclose = event => {
    console.log('Connection Closed', event)
    if (!event.wasClean && event.code == 1006) {
      $('#error').innerText = 'Could not connect to server'
    }
    $('#error').innerText = 'Connection to server lost'
    $('#note').disabled = true
  }
  
  ws.onmessage = event => {
    let { type, data } = JSON.parse(event.data)
    switch (type) {
      case 'filechange':
        data.hasfile ? show('#files') : hide('#files')
        break
      case 'welcome':
        $('#users').innerText = data.users
        if (data.hasfile) {
          show('#files')
        }
        data = data.note
        if (firstTime) {
          firstTime = false
          plausible('pageview', {props: {has_content: data.data != ''}})
        }
      // fallthrough
      case 'update':
        $('#note').value = decodeURIComponent(atob(data.data))
        if ($('#note').value.trim() != '') {
          backupText = ''
          $('#lastModified').innerText = new Date(data.timestamp).toLocaleString()
        }
        scanInput(true)
        break
      default:
        console.log(event)
    }
  }
}

setupSocket()

/////////////////////////////////
//    Constants & Helpers      //
/////////////////////////////////
const API = '/snippet'

const $ = x => document.querySelector(String(x))

/**
 * Make elements have active class for a few extra moments
 */
const buttonPress = x => {
  $(x).className += ' active'
  setTimeout(
    () => ($(x).className = $(x).className.replace(' active', '')),
    1500
  )
}

const show = x => {
  let e = $(x)
  e.style.opacity = '1'
  e.style.cursor = 'pointer'
  e.style.transition = '1s'
  e.disabled = false
}

const hide = x => {
  let e = $(x)
  e.style.opacity = '0'
  e.style.cursor = 'default'
  e.style.transition = '1s'
  e.disabled = true
}

// Check if the input is a url
const hasLink = x =>
  !x.includes('\n') &&
  x.includes('http') &&
  x.includes('://') &&
  x.includes('.')

// Update button visibility based on the input's value
const scanInput = (skipSend = false) => {
  var text = $('#note').value

  if (ws && !skipSend) {
    ws.send(
      JSON.stringify({
        type: 'data',
        data: btoa(encodeURIComponent(text))
      })
    )
  }

  if (text.trim() == '') {
    $('#lastModified').innerText = ''
  }

  // Check for the existance of a link
  hasLink(text) ? show('#followlink') : hide('#followlink')

  // Check if there is any value in the box
  text != ''
    ? show('#cleartext') || show('#copytext')
    : hide('#cleartext') || hide('#copytext')
}

$('#note').oninput = () => scanInput()

// post the note to the backend
let submit = e => {
  e.preventDefault()
  return false
}

$('#form').onsubmit = submit

document.addEventListener("visibilitychange", (event) => {
  if (document.visibilityState == "visible" && $('#note').disabled) {
    // console.log("tab is now active, reconnecting")
    setupSocket()
  }
});

/////////////////////////////////
//    Button Press Behaivors   //
/////////////////////////////////

// used in undoing clear
var backupText = ''
// clear the input field and reset backend
const clear = e => {
  buttonPress('#cleartext')
  backupText = $('#note').value
  $('#note').value = ''
  scanInput()
  plausible('Clear')
}

$('#cleartext').onclick = clear

// cope value to clipboard
$('#copytext').onclick = e => {
  buttonPress('#copytext')
  $('#note').select()
  plausible('Copy')
  document.execCommand('copy')
}

// follow links if they are valid
$('#followlink').onclick = $('#followlink').oncontextmenu = e => {
  let text = $('#note').value

  plausible('Follow Link', {callback: () => {
      // clear after if right clicked
      if (e.type == 'contextmenu') clear()
      if (hasLink(text)) window.location.href = text
  }})
}

// go to files access url
$('#files').onclick = e => {
  plausible('Access File', {callback: () => (window.location.href = '/files')})
}

$('#files').oncontextmenu = e => {
  deleteFile()
  plausible('Delete File')
  $('#gallery').textContent = ''
  return false
}
/////////////////////////////////
//        Zen Mode Logic       //
/////////////////////////////////
const enhanceZenMode = () => {
  var locationHashPrefix = 'expand_'
  var model = { activeZenWidget: null }

  var updateActiveZenWidget = checkboxForActiveZenWidget => {
    var checkbox = checkboxForActiveZenWidget
    checkbox.checked = true
    model.activeZenWidget = {
      checkbox: checkbox,
      textarea: $('#' + checkbox.id.split('-').pop())
    }

    model.activeZenWidget.textarea.focus()
    window.location.hash = locationHashPrefix + checkbox.id
  }

  var exitZenMode = () => {
    if (model.activeZenWidget != null) {
      model.activeZenWidget.checkbox.checked = false
      model.activeZenWidget = null
      window.location.hash = ''
    }
  }

  var watchCheckboxChangesAndUpdateZenMode = () => {
    $('.zennable input[type=checkbox]').onchange = event => {
      var changedCheckbox = event.currentTarget
      if (changedCheckbox.checked) {
        updateActiveZenWidget(changedCheckbox)
      } else {
        exitZenMode()
      }
    }
  }

  var getCheckboxFromLocationHash = () => {
    var checkboxId = window.location.hash
      .replace('#' + locationHashPrefix, '')
      .trim()

    return checkboxId ? $('.zennable #' + checkboxId) : null
  }

  var watchLocationHashAndUpdateZenMode = () => {
    var handleHashChange = () => {
      var checkbox = getCheckboxFromLocationHash()
      if (checkbox) {
        updateActiveZenWidget(checkbox)
      } else {
        exitZenMode()
      }
    }

    window.onhashchange = handleHashChange

    handleHashChange()
  }

  watchCheckboxChangesAndUpdateZenMode()
  watchLocationHashAndUpdateZenMode()

  document.onkeydown = e => {
    if (e.key == 'Escape') {
      window.location.hash == '' ? $('#note').blur() : exitZenMode()
    }

    if (e.keyCode == 90 && e.ctrlKey && backupText != '') {
      $('#note').value = backupText
      backupText = ''
      scanInput()
    }

    if (
      (!e.altKey && !e.ctrlKey && !e.metaKey && !e.shiftKey) ||
      e.key === 'Meta' ||
      e.key === 'Shift' ||
      e.key === 'Control' ||
      e.key === 'alt'
    ) {
      return
    }
  }
}

enhanceZenMode()
