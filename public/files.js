let dropArea = $('#dropArea')

function preventDefaults (e) {
  e.preventDefault()
  e.stopPropagation()
}

// Paste & Right click Handlers
window.addEventListener('paste', e => {
  // Text or file
  var d = e.clipboardData
  if (d.files.length != 0) {
    console.log('Uploading file from paste')
    handleFiles(d.files)
  }
})

function handleRightClickUpload (e) {
  if (e.target == document.body) {
    preventDefaults(e)
    $('#file-input').click()
  }
}

;['contextmenu'].forEach(eventName => {
  dropArea.addEventListener(eventName, handleRightClickUpload, false)
})

// Normal drag & drop handlers
;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName =>
  dropArea.addEventListener(eventName, preventDefaults, false)
)

function highlight (e) {
  dropArea.classList.add('highlight')
}

;['dragenter', 'dragover'].forEach(eventName =>
  dropArea.addEventListener(eventName, highlight, false)
)

function unhighlight (e) {
  dropArea.classList.remove('highlight')
}

;['dragleave', 'drop'].forEach(eventName =>
  dropArea.addEventListener(eventName, unhighlight, false)
)

function handleDrop (e) {
  let dt = e.dataTransfer
  let files = dt.files
  handleFiles(files)
}

dropArea.addEventListener('drop', handleDrop, false)

function handleFiles (files) {
  ;[...files].forEach(uploadFile)
}

function uploadFile (file) {
  let formData = new FormData()
  formData.append('file', file)

  fetch('/files', {
    method: 'POST',
    body: formData
  })
    .then(() => {
      previewFile(file)
    })
    .catch(e => {
      console.log(e)
    })
}

function deleteFile () {
  fetch('/files', {
    method: 'DELETE'
  }).catch(e => {
    console.log(e)
  })
}

function previewFile (file) {
  let reader = new FileReader()
  reader.readAsDataURL(file)
  reader.onloadend = function () {
    let gallery = $('#gallery')
    gallery.textContent = ''

    let title = document.createElement('div')
    title.textContent = `Uploaded: ${file.name}`
    gallery.appendChild(title)

    if (reader.result.includes('image/')) {
      let img = document.createElement('img')
      img.src = reader.result
      gallery.appendChild(img)
    }
  }
}
