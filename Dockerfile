FROM denoland/deno:alpine-1.41.3

RUN apk add --no-cache tzdata
ENV TZ=Europe/Copenhagen
ENV TZ 'America/New_York'

EXPOSE 8082
WORKDIR /app
RUN chown -R deno /app
USER deno

COPY index.ts .
RUN deno cache index.ts
COPY public public

CMD ["run", "--allow-env=SCRIPTS_URL", "--allow-net", "--allow-read=.", "--allow-write=.",  "index.ts"]
