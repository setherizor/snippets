import type {
  WebSocket,
  WebSocketCloseEvent,
} from 'https://deno.land/std@0.99.0/ws/mod.ts';
import { acceptWebSocket } from 'https://deno.land/std@0.99.0/ws/mod.ts';
import type { FormFile } from 'https://deno.land/std/mime/multipart.ts';

import {
  Application,
  Context,
  HandlerFunc,
  HttpException,
  MiddlewareFunc,
  NotFoundException,
} from 'https://deno.land/x/abc@v1.3.3/mod.ts';

// Consts
const app = new Application();
const note = { data: '', timestamp: new Date() };
const hostname = Deno.env.get('SCRIPTS_URL') || 'https://setherizor.gitlab.io/';
const scriptsPath = '/scripts/';
const port = 8082;

/**
 * Snippet storage and retrieval logic
 */
export const snippet: HandlerFunc = async (c: Context) => {
  if (c.request.method == 'POST') {
    if (c.request.headers.get('content-type') !== 'text/plain;charset=utf-8') {
      throw new HttpException('Invalid body', 422);
    }
    const body = await c.body as string;
    note.data = body;
    note.timestamp = new Date();
  }

  if (note.data != '') {
    c.response.headers.set(
      'Last-Modified',
      note.timestamp.toLocaleDateString('en-US', {
        month: 'long',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
      }),
    );
  }

  return note.data;
};

// deno-lint-ignore no-explicit-any
let file_store: any = null;

/**
 * Way to handle actual files
 */
export const files: HandlerFunc = async (c: Context) => {
  // if more than a 1.5 GB, ignore the request
  if (c.request.contentLength != null && c.request.contentLength > 1500000000) {
    return 'file bigger than';
  }

  const hasTempFile = file_store != null && file_store.tempfile != undefined &&
    file_store.tempfile != null;

  // If new file, read and store (libs handle large uploads with temp files)
  //  delete current tempfile if exists
  if (['POST', 'PUT'].includes(c.request.method)) {
    if (hasTempFile) {
      await Deno.remove(file_store.tempfile);
    }
    const { file } = await c.body as { file: FormFile };
    file_store = file;
    await updateFile();
  }

  if (['DELETE'].includes(c.request.method)) {
    if (hasTempFile) {
      await Deno.remove(file_store.tempfile);
    }
    file_store = null;
    await updateFile();
  }

  // Otherwise serve up stored files
  if (['GET'].includes(c.request.method) && file_store != null) {
    c.response.headers.append(
      'Content-Disposition',
      `inline; filename="${file_store.filename}"`,
    );
    if (hasTempFile) {
      await c.file(file_store.tempfile);
    } else {
      c.blob(file_store.content, file_store.type);
    }
  }

  return '';
};

/**
 * Snippet storage and retrieval logic for command line interfaces
 */
export const cli: HandlerFunc = async (c: Context) => {
  if (['POST', 'PUT'].includes(c.request.method)) {
    const body = await c.body as string;
    note.data = btoa(encodeURIComponent(body));
    note.timestamp = new Date();
  }

  sendall({
    type: 'update',
    data: note,
  });

  return decodeURIComponent(atob(note.data));
};

let sockets: WebSocket[] = [];

// deno-lint-ignore no-explicit-any
async function sendall(obj: Record<string, any>) {
  const message = JSON.stringify(obj);
  sockets = sockets.filter((s) => !s.isClosed);
  await Promise.allSettled(sockets.map((w) => w.send(message)));
}

async function updateFile() {
  await sendall({
    type: 'filechange',
    data: {
      hasfile: file_store != null && file_store.filename != null,
    },
  });
}

async function updateUsers() {
  sockets = sockets.filter((s) => !s.isClosed);
  await sendall({
    type: 'welcome',
    data: {
      users: 'Others: ' + (sockets.length - 1),
      note,
      hasfile: file_store != null && file_store.filename != null,
    },
  });
}

const socketHandler: HandlerFunc = async (c: Context) => {
  const { conn, headers, r: bufReader, w: bufWriter } = c.request;
  const ws = await acceptWebSocket({
    conn,
    headers,
    bufReader,
    bufWriter,
  });

  sockets.push(ws);
  updateUsers();

  for await (const e of ws) {
    if ((e as WebSocketCloseEvent).code == 1001) {
      updateUsers();
    } else {
      const { type, data } = JSON.parse(e as string);
      switch (type) {
        case 'data':
          note.data = data;
          note.timestamp = new Date();
          sendall({
            type: 'update',
            data: note,
          });
          break;
        default:
          console.log(e);
          await ws.send('Hello, Client!');
      }
    }
  }
};

/**
 * Performs requests to scripts host
 */
export const scriptsProxy: MiddlewareFunc =
  (next: HandlerFunc) => async (c: Context) => {
    try {
      const internalPaths = ['snippet', 'ws', '/c', '/files'];
      if (
        internalPaths.some((x) => c.path.endsWith(x)) ||
        await Deno.realPath('public' + c.path)
      ) {
        return next(c);
      }
    } catch (_error) {
      c.response.headers.delete('x-frame-options');
      const res = await fetch(hostname + scriptsPath + c.request.url);
      if (res.status == 404) {
        throw new NotFoundException('Sorry, I couldn\'t find that');
      }
      return await res.text();
    }
  };

app
  .use(scriptsProxy)
  .any('/files', files)
  .any('/c', cli)
  .any('/snippet', snippet)
  .static('/', 'public')
  .file('/', 'public/index.html')
  .any('/ws', socketHandler)
  .start({ port });

console.log('Seth\'s Snippets running on port: http://localhost:' + port);
