# Seth's Snippets

A useful little app for

- sharing snippets & plaintext files
- proxying requests to a repo of useful scripts and files I have on
  (Gitlab)[https://gitlab.com/setherizor/scripts]
- checking on my VPN's clients

## Technologies

- [Deno](https://deno.land/)
- [Velociraptor](https://velociraptor.run/docs/introduction/)

## Usage

### Curl

```sh
# Write
echo hello world | curl -LsT - s.sethp.cc/c

# Read
curl -Ls s.sethp.cc/c

# Write File
FILE=$PWD/Seth\ Entry\ Form.pdf; curl -iL s.sethp.cc/files --form "file=@$FILE"
```
